package test;

import java.awt.Color;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.awt.image.MemoryImageSource;
import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import net.sourceforge.tess4j.Tesseract;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.NicelyResynchronizingAjaxController;
import com.gargoylesoftware.htmlunit.ScriptResult;
import com.gargoylesoftware.htmlunit.SilentCssErrorHandler;
import com.gargoylesoftware.htmlunit.ThreadedRefreshHandler;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlImage;
import com.gargoylesoftware.htmlunit.html.HtmlInput;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

public class Test {

    public static void main(String[] args) throws Exception {
        homePage_proxy("2112310666711","胡志鹏");
    }

    public static void homePage_proxy(String eticketNoORIn,String passengerName) throws Exception {
//        WebClient webClient = new WebClient(BrowserVersion.FIREFOX_24, "192.168.2.49", 8080);
//     // set proxy username and password
//        final DefaultCredentialsProvider credentialsProvider = (DefaultCredentialsProvider) webClient
//                .getCredentialsProvider();
//        credentialsProvider.addCredentials("dong.tl", "~!@Tonglin3");

        final WebClient webClient = new WebClient(BrowserVersion.CHROME);
        webClient.setAjaxController(new NicelyResynchronizingAjaxController());
        webClient.setJavaScriptTimeout(5000);
        webClient.waitForBackgroundJavaScript(5000);
        webClient.setRefreshHandler(new ThreadedRefreshHandler());
        webClient.setCssErrorHandler(new SilentCssErrorHandler());

        webClient.getOptions().setJavaScriptEnabled(true);
        webClient.getOptions().setCssEnabled(true);
        webClient.getOptions().setRedirectEnabled(false);
        webClient.getOptions().setThrowExceptionOnScriptError(false);
        webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
        webClient.getOptions().setTimeout(5000);
//        webClient.getOptions().setUseInsecureSSL(true);

        HtmlPage page = webClient.getPage("http://www.travelsky.com/tsky/validate.jsp");

//        System.out.println(page.asXml());
//System.out.println(((HtmlInput) page.getByXPath("/html/body/div[2]/div[1]/div/form/div[5]/div[2]/input").get(0)).getAttribute("value"));
//        if(1==1)return;
        ((HtmlInput) page.getElementById("eticketNoORIn")).setValueAttribute(eticketNoORIn.replace("-", ""));
        ((HtmlInput) page.getElementById("passengerName_src")).setValueAttribute(passengerName);
        ((HtmlInput) page.getElementById("passengerName")).setValueAttribute(passengerName);

        final HtmlImage image = (HtmlImage) page.getElementById("img_randCode_t");

        ImageReader imageReader = image.getImageReader();
        BufferedImage bufferedImage = imageReader.read(0);
        
        
        ImageIO.write(bufferedImage, "jpg", new File("2.jpg"));
        Pic pic = new Pic("2.jpg");
        BufferedImage imagea=tessT.toBufferedImage(Toolkit.getDefaultToolkit().createImage( new MemoryImageSource(pic.getWidth(),pic.getHeight(),pic.getRemovedNoise2(),0,pic.getWidth())));
        ImageIO.write(imagea,"jpeg",new File("1.jpg"));
        imagea = bufferedImage;
        int minY=0;
        int height=40;
        int minX=0;
        int width=90;
        for (int y = minY; y < height; y++) {  
            for (int x = minX; x < width; x++) {  
                int rgb = imagea.getRGB(x, y);  
                Color color = new Color(rgb); // 根据rgb的int值分别取得r,g,b颜色。  
                int gray = (int) (0.3 * color.getRed() + 0.59  
                    * color.getGreen() + 0.11 * color.getBlue());  
                Color newColor = new Color(gray, gray, gray);  
                imagea.setRGB(x, y, newColor.getRGB());  
            }  
        }
        for (int y = minY; y < height; y++) {  
            for (int x = minX; x < width; x++) {  
                int rgb = imagea.getRGB(x, y);  
                Color color = new Color(rgb); // 根据rgb的int值分别取得r,g,b颜色。  
                Color newColor = new Color(255 - color.getRed(), 255 - color  
                    .getGreen(), 255 - color.getBlue());  
                imagea.setRGB(x, y, newColor.getRGB());  
            }  
        }  
        for (int y = minY; y < height; y++) {  
            for (int x = minX; x < width; x++) {  
                int rgb = imagea.getRGB(x, y);  
                Color color = new Color(rgb); // 根据rgb的int值分别取得r,g,b颜色。  
                int value = 255 - color.getBlue();  
                if (value > 122) {  
                    Color newColor = new Color(0, 0, 0);  
                    imagea.setRGB(x, y, newColor.getRGB());  
                } else {  
                    Color newColor = new Color(255, 255, 255);  
                    imagea.setRGB(x, y, newColor.getRGB());  
                }  
            }  
        }
       
        ImageIO.write(imagea,"jpeg",new File("1.jpg"));
        Tesseract instance = new Tesseract();
//        instance.setDatapath(new File(".").getPath());
        //Tesseract1 instance = new Tesseract1();
        String result2 = instance.doOCR(imagea);
        
        Matcher m = Pattern.compile(
                "[A-Z,a-z,0-9]")
                .matcher(result2);
        StringBuilder sb = new StringBuilder();
        while (m.find()) {
            sb.append(m.group());
        }
        
        System.out.println("----------"+sb);
        
        
        JFrame f2 = new JFrame();
        JLabel l = new JLabel();
        l.setIcon(new ImageIcon(bufferedImage));
        f2.getContentPane().add(l);
        f2.setSize(100, 100);
        f2.setTitle("验证码");
        f2.setVisible(true);
        String valicodeStr = JOptionPane.showInputDialog("请输入验证码：");
        f2.setVisible(false);

        ((HtmlInput) page.getElementById("randCode")).setValueAttribute(valicodeStr);

        HtmlInput butten = (HtmlInput) page.getByXPath("/html/body/div[2]/div[1]/div/form/div[5]/div[2]/input").get(0);
        System.out.println(butten.getAttribute("value"));

        String js = "";
        if(eticketNoORIn.length()==13||eticketNoORIn.length()==14){
            js = "$('#eticketNo').val($.trim($('#eticketNoORIn').val()));$('#validateFlag').val(0);document.getElementById(\"validateForm\").submit();";
        }else{
            js = "$('#invoiceNo').val($.trim($('#eticketNoORIn').val()));$('#validateFlag').val(1);document.getElementById(\"validateForm\").submit();";
        }

//        ScriptResult result = page
//                .executeJavaScript("$('#eticketNo').val($.trim($('#eticketNoORIn').val()));$('#validateFlag').val(0);document.getElementById(\"validateForm\").submit();");
        
        ScriptResult result = page
                .executeJavaScript(js);
        
        HtmlPage page2 = (HtmlPage) result.getNewPage();
        Thread.sleep(2000);
//        HtmlElement div = (HtmlElement) page2.getByXPath("/html/body/div[2]/div[2]/div[2]").get(0);
//
//        for (int i = 0; i < 2; i++) {
//            System.out.println(div.asXml());
//            if (div.getElementsByTagName("img").size() == 0) {
//                System.out.println(i + ":" + div.asXml());
//                break;
//            }
//
//            synchronized (page2) {
//                page2.wait(500);
//            }
//        }
        
        System.out.println(page2.asXml());
        System.exit(0);
    }
}
